import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { User } from '../models/user';
import { HttpserviceService } from '../services/httpservice.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  public alertShow: boolean = false;
  public alertMsg: string = '';
  public username = 'sumit';
  public email: any;
  /////////////////////////
  dropdownList: any = [];
  selectedItems: any = [];
  dropdownSettings: IDropdownSettings = {};
  //////////////////////
  public userI: number = 0;

  constructor(
    private router: Router,
    private userService: HttpserviceService,
    public user: User,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.email = localStorage.getItem('email');
      this.userService.getUserData(this.email).subscribe((response) => {
        console.log(response);
        this.user.friends = response.friends;
        this.user.finalSettlement = response.finalSettlement;
        this.user.groups = response.groups.filter((group: any) => {
          return !group[1].includes(' and ');
        });
        console.log(this.user.groups);
        this.user.username = response.username;
        ////////
        this.dropdownList = this.user.friends.map((friend: any) => {
          return { item_id: friend[0], item_text: friend[1] };
        });
      });
    });
    ///////////
    this.selectedItems = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
    };
    //////////
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  addGroupData(groupName: any, multiselect: any) {
    let userId = localStorage.getItem('userId');
    const userId2 = Number(userId);

    multiselect = multiselect.map((element: any) => {
      return element.id;
    });
    multiselect.push(userId2);
    this.userService
      .postAddGroup(groupName, multiselect)
      .subscribe((response: any) => {
        this.userService.getUserData(this.email).subscribe((response) => {
          console.log(response);
          this.user.friends = response.friends;
          this.user.finalSettlement = response.finalSettlement;
          this.user.groups = response.groups.filter((group: any) => {
            return !group[1].includes(' and ');
          });
          console.log(this.user.groups);
          this.user.username = response.username;
        });
      });
  }

  getDashboardData() {
    this.router.navigate(['/logged/dashboard']);
  }

  getAllExpenses() {
    this.router.navigate(['/logged/all']);
  }

  getGroup(groupId: string) {
    this.router.navigate(['logged/group', groupId]);
  }

  getFriend(friend: any) {
    this.router.navigate(['logged/friend', friend[0], friend[1]]);
  }

  addExpenseData(amount: any, desc: any, friend: any) {
    const userId = localStorage.getItem('userId');
    this.userService
      .postFriendExpense(userId, amount, desc, friend)
      .subscribe((res: any) => {
        console.log(res);
        this.alertMsg = 'Expense Added Successfully!';
        this.alertShow = true;
        setTimeout(() => {
          this.alertShow = false;
        }, 2000);
        this.router.navigate(['logged']);
      });
    console.log(amount, desc, friend);
  }
  addFriendData(email: any) {
    const userId = localStorage.getItem('userId');
    this.userService.postAddFriend(userId, email).subscribe((res: any) => {
      this.alertMsg = res.message;
      if (this.alertMsg === 'Friend Not Registered!!') {
        this.alertShow = true;
        setTimeout(() => {
          this.alertShow = false;
        }, 3000);
      }
      console.log(res);
      this.email = localStorage.getItem('email');
      this.userService.getUserData(this.email).subscribe((response) => {
        console.log(response);
        this.user.friends = response.friends;
        this.user.finalSettlement = response.finalSettlement;
        this.user.groups = response.groups.filter((group: any) => {
          return !group[1].includes(' and ');
        });
        console.log(this.user.groups);
        this.user.username = response.username;
      });
    });
    console.log(email);
  }
}
