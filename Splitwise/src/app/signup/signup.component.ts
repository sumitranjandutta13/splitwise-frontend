import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignUpUser } from 'models/signUpUser';
import { HttpserviceService } from '../services/httpservice.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  public alertShow: boolean = false;
  public alertMsg: string = '';
  constructor(
    private sign: SignUpUser,
    private APIservice: HttpserviceService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  valid: any = false;
  SignUp(username: any, email: any, password: any) {
    if (username == '' && email == '' && password == '') {
      return;
    } else if (
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email
      )
    ) {
      this.valid = true;
    } else if (!this.valid) {
      this.alertMsg = 'Email not Valid';
      this.alertShow = true;
      setTimeout(() => {
        this.alertShow = false;
      }, 2000);
      return;
    }

    console.log(username, email, password);
    this.sign.username = username;
    this.sign.email = email;
    this.sign.password = password;
    this.APIservice.createSignUpPost(this.sign).subscribe((res: any) => {
      console.log(res);
      if (res.message === 'ok') {
        this.router.navigate(['/login']);
      } else {
        this.alertMsg = res.message;
        this.alertShow = true;
        setTimeout(() => {
          this.alertShow = false;
        }, 2000);
      }
    });
  }
}
