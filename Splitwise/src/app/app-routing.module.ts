import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllExpenseComponent } from './all-expense/all-expense.component';
import { DashboardOverviewComponent } from './dashboard-overview/dashboard-overview.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FriendDataComponent } from './friend-data/friend-data.component';
import { GroupDataComponent } from './group-data/group-data.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },

  { path: 'logged', redirectTo: '/logged/dashboard', pathMatch: 'full' },
  {
    path: 'logged',
    component: DashboardComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardOverviewComponent,
      },
      {
        path: 'all',
        component: AllExpenseComponent,
      },
      { path: 'group/:groupId', component: GroupDataComponent },
      { path: 'friend/:friendId/:friendName', component: FriendDataComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
