import { Component, OnInit } from '@angular/core';
import { HttpserviceService } from '../services/httpservice.service';
import { UserLogin } from 'models/userLogin';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public alertShow: boolean = false;
  public alertMsg: string = '';
  constructor(
    private user: UserLogin,
    private Apiservice: HttpserviceService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  valid: any = false;
  login(email: any, password: any) {
    if (email === '' || password === '') {
      return;
    } else if (
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        email
      )
    ) {
      this.valid = true;
    } else if (!this.valid) {
      this.alertMsg = 'Email not Valid';
      this.alertShow = true;
      setTimeout(() => {
        this.alertShow = false;
      }, 2000);
      return;
    }

    console.log(email, password);
    this.user.email = email;
    this.user.password = password;
    this.Apiservice.createLoginPost(this.user).subscribe((res: any) => {
      console.log(res);

      if (res.message === 'token created') {
        localStorage.setItem('token', res.signature);
        localStorage.setItem('email', res.email);
        localStorage.setItem('userId', res.userID);
        this.router.navigate(['/logged']);
      } else {
        this.alertMsg = res.message;
        this.alertShow = true;
        setTimeout(() => {
          this.alertShow = false;
        }, 2000);
      }
    });
  }
}
